\documentclass[journal]{IEEEtran}

% *** GRAPHICS RELATED PACKAGES ***
%
\ifCLASSINFOpdf
   \usepackage[pdftex]{graphicx}
  % declare the path(s) where your graphic files are
   \graphicspath{{./drawings/}}
  % and their extensions so you won't have to specify these with
  % every instance of \includegraphics
  % \DeclareGraphicsExtensions{.pdf,.jpeg,.png}
\else
  % or other class option (dvipsone, dvipdf, if not using dvips). graphicx
  % will default to the driver specified in the system graphics.cfg if no
  % driver is specified.
  % \usepackage[dvips]{graphicx}
  % declare the path(s) where your graphic files are
  % \graphicspath{{../eps/}}
  % and their extensions so you won't have to specify these with
  % every instance of \includegraphics
  % \DeclareGraphicsExtensions{.eps}
\fi

% correct bad hyphenation here
\hyphenation{op-tical net-works semi-conduc-tor}


\begin{document}
%
% paper title
% Titles are generally capitalized except for words such as a, an, and, as,
% at, but, by, for, in, nor, of, on, or, the, to and up, which are usually
% not capitalized unless they are the first or last word of the title.
% Linebreaks \\ can be used within to get better formatting as desired.
% Do not put math or special symbols in the title.
\title{Low Energy Heterogeneous Computing with Multiple \mbox{RISC-V} and CGRA cores}
%
%
% author names and IEEE memberships
% note positions of commas and nonbreaking spaces ( ~ ) LaTeX will not break
% a structure at a ~ so this keeps an author's name from being broken across
% two lines.
% use \thanks{} to gain access to the first footnote area
% a separate \thanks must be used for each paragraph as LaTeX2e's \thanks
% was not built to handle multiple paragraphs
%

% \author{Lu\'is Fiolhais, Maxim Hariton, Jo\~ao D. Lopes, Fernando Gon\c calves, Rui P. Duarte, M\'ario V\'estias, Jos\'e T. de Sousa}% <-this % stops a space


% note the % following the last \IEEEmembership and also \thanks - 
% these prevent an unwanted space from occurring between the last author name
% and the end of the author line. i.e., if you had this:
% 
% \author{....lastname \thanks{...} \thanks{...} }
%                     ^------------^------------^----Do not want these spaces!
%
% a space would be appended to the last name and could cause every name on that
% line to be shifted left slightly. This is one of those "LaTeX things". For
% instance, "\textbf{A} \textbf{B}" will typeset as "A B" not "AB". To get
% "AB" then you have to do: "\textbf{A}\textbf{B}"
% \thanks is no different in this regard, so shield the last } of each \thanks
% that ends a line with a % and do not let a space in before the next \thanks.
% Spaces after \IEEEmembership other than the last one are OK (and needed) as
% you are supposed to have spaces between the names. For what it is worth,
% this is a minor point as most people would not even notice if the said evil
% space somehow managed to creep in.

% make the title area
\maketitle

% As a general rule, do not put math, special symbols or citations
% in the abstract or keywords.
\begin{abstract}
The idea of combining multiple CPU and CGRA cores is not in itself original in
academia but detailed characterizations of such architectures and measurements
on compelling applications are difficult to find. Although commercial CPUs, GPUs
and FPGAs are widely available, there are no commercial CGRAs, which may be
attributed to the lack of metrics on performance, energy and cost. In this paper
we introduce a heterogeneous computing platform consisting of several
\mbox{RISC-V} CPU and Versat CGRA cores. Implementation results for several
instances of the architecture are presented. The CPU of choice is the promising
open source \mbox{RISC-V} architecture, which has never been featured in
CPU/CGRA architectures. This paper presents independent implementations of two
\mbox{RISC-V} cores: a minimal one, useful as a simple controller, and a more
performant 5-stage pipeline implementation. The \mbox{RISC-V} cores have been
designed using the recent Chisel HDL, useful for automating tasks pertaining to
the writing of RTL. The selected CGRA is the published Versat architecture, for
which 4 different instances have been created. Implementation results for 2 FPGA
families and several ASIC technology nodes are presented: area, frequency and
power. Applications cover digital audio and machine learning, demonstrating the
versatility of the proposed platform at very competitive area, frequency and
energy footprints.
\end{abstract}

% Note that keywords are not normally used for peerreview papers.
\begin{IEEEkeywords}
Low power architectures, \mbox{RISC-V}, CGRAs, Heterogeneous computing
\end{IEEEkeywords}


% For peer review papers, you can put extra information on the cover
% page as needed:
% \ifCLASSOPTIONpeerreview
% \begin{center} \bfseries EDICS Category: 3-BBND \end{center}
% \fi
%
% For peerreview papers, this IEEEtran command inserts a page break and
% creates the second title. It will be ignored for other modes.
\IEEEpeerreviewmaketitle



\section{Introduction}

\IEEEPARstart{W}{ith} the advent of the Internet of Things (IoT) and Artificial
Intelligence (AI) algorithms based on neural networks, it is becoming more and
more important to reduce the cost and energy consumption of silicon devices.
Systems using high performance CPUs, (heterogeneous) FPGAs, combinations of CPUs
and GPUs~\cite{Mittal15} or CPUs and embedded FPGAs~\cite{Qiu16} are interesting
but can hardly fit the bill in applications requiring ubiquitous low cost and
low power devices.

A high performance CPU can do a few operations in parallel but most of its
hardware is dedicated to handling instructions efficiently, which represents a
large overhead for the envisioned systems. FPGAs are designed to build arbitrary
digital circuits and require a formidable configuration infrastructure which is
an overkill for low energy IoT systems. GPUs comprise large numbers of
streamlined von Neumann processors, requiring a lot more hardware than a
dedicated hardware datapath for the same purpose. Finally, a system entirely
implemented in hardware would probably have the best performance but at the cost
of a large silicon area (high device cost) and lack of programmability.

Given the drawbacks of the above approaches, an interesting option seems to be a
system consisting of several simple CPU cores combined with programmable
hardware cores that are simpler than FPGAs. For algorithms that require
intensive control, the use of CPUs actually represents an economy of resources
as control structures can be scheduled and executed in a single CPU. This
corresponds to time multiplexing of control tasks in a small piece of hardware;
for more tasks more CPUs can be added. Reasoning in a similar way, the data
processing tasks can be time multiplexed using the programmable hardware: large
hardware circuits can be broken down into smaller pieces and run on programmable
hardware.

Because FPGAs are too onerous for most applications, a more suitable type of
reconfigurable hardware for embedded devices is the Coarse-Grained
Reconfigurable Array (CGRA)~\cite{deSutter10}. A CGRA is a collection of
programmable functional units (FUs) interconnected by programmable switches. The
FUs operate on data words of commonly used widths such as 8, 16, 32 or 64-bit
words. When the CGRA is programmed it implements hardware datapaths that
accelerate computations.

In this work, a heterogeneous computing platform consisting of several
\mbox{RISC-V} CPUs~\cite{riscv-isa-manual} and several Versat
CGRAs~\cite{Lopes16} is proposed. The \mbox{RISC-V} core has been developed
under a project code named Adept. A similar approach has been proposed
in~\cite{hussain2014} but here we make the case for the use of \mbox{RISC-V}
CPUs and Versat CGRAs.

In order to sustain the IoT revolution and unleash the creativity of millions of
engineers, it is indispensable to have an Instruction Set Architecture that is
free of Intellectual Property (IP) rights or license
fees~\cite{asanovic14}. Among the few free Instruction Set Architectures (ISAs)
available, \mbox{RISC-V} seems to be the most promising one, with wide backing
from academia and industry. The other available open source solutions are either
dependent on a specific technology, have no parametrization options, or have
non-robust development environments~\cite{jts:rec14}. Some open source
processors are often dependent on a specific non-free toolchain, simulator or
FPGA that is used to demonstrate the capacities of these processors. However,
these initiatives tend to fade out if there is not a strong ecosystem to support
them.

With the emergence of \mbox{RISC-V}~\cite{riscv-isa-manual} and its open source
toolchain, with special emphasis on the Chisel3 Hardware Description Language
(HDL)~\cite{Bachrach:DAC2012} and the Rocket Chip SoC
Generator~\cite{Asanovic:EECS-2016-17}, it is likely that the success of open
source OS's is extended to open CPU hardware descriptions.

A previous attempt to create a System on Chip (SoC) called Blackbird, and a
programming environment for building reconfigurable systems using the OpenRISC
open source processor has been made~\cite{jts:rec14}. Blackbird was intended to
become an independent SoC derived from ORPSoC~\cite{orpsoc}, the OpenRISC
equivalent of the Rocket Chip SoC Generator~\cite{Asanovic:EECS-2016-17}.
Unfortunately, this effort failed as the budget ran out, mainly because there
was always dependencies on particular FPGA boards or proprietary EDA tools which
could not be supported in the Blackbird project. Also, the OpenRISC initiative
has failed to create a mature enough ecosystem of its own, and the effort
revealed itself too complex, as many essential parts of the system were missing
and/or were still being developed by the small existing community. In fact, in
spite of the initial enthusiasm, the same may happen to \mbox{RISC-V}. However,
the recent impetus of the \mbox{RISC-V} initiative is far stronger than OpenRISC
has ever enjoyed. Moreover, crowd development is becoming increasingly easier
with the advent of web platforms such as {\tt github.com}.

The effort reported in~\cite{jts:rec14} has been recently continued with
\mbox{RISC-V} processors, with a fresh attempt to build a new SoC called
Warpbird~\cite{lf:rec18}. Unfortunately, the team has run into similar
difficulties as when using the OpenRISC core, which led to the identification of
a new research problem thereby called the Tethered SoC Problem (TSP). The TSP
problem occurs when the modules of an open source SoC can not be used in a new
context (FPGA or ASIC design flow) different from the one where they have been
originally used. This problem has a similar connotation in software when using
non-generic functions/classes. Thus, the work in~\cite{lf:rec18} was called an
attempt to build an Untethered SoC.

The difficulties have been partially overcome by writing a \mbox{RISC-V} core
from scratch while still using the Chisel HDL. The system proposed in this paper
is portable to any FPGA or ASIC technology as Chisel generates synthesizable
Verilog code. The \mbox{RISC-V} CPU is supported by the GNU toolchain and the
Versat CGRA has its own compiler and assembler~\cite{Santiago2017}.

\section{Architecture}

The proposed architecture can be used to develop SoCs containing several Adept
\mbox{RISC-V} cores and Versat CGRAs. An example system consisting of 4
\mbox{RISC-V} cores and 2 Versat CGRAs is depicted in
Fig.~\ref{fig_top_sys}. The system uses ARM's Advanced eXtensible Interface
(AXI) to interconnect the cores, a de facto standard for busing.

\begin{figure}[!t]
\centering
\includegraphics[width=2.5in]{top_sys}
\caption{Block diagram of 4-CPU/2-CGRA instance}
\label{fig_top_sys}
\end{figure}

In the figure, two CPU cores share the same CGRA, which has a 2-port AXI slave
interface connected to the AXI master interfaces of its hosts. The CGRA
arbitrates between the two hosts using a round-robin scheme. All CPU and CGRA
cores access the external memory by means of their AXI master interfaces
connected to an AXI slave multi-port memory controller core, which is a 3rd
party core. The external memory must be loaded with programs and data for all
cores. This is done by the host CPU, which runs a boot loader program and is
able to fetch this data from an external device by means of an AXI or SPI
peripheral, not shown in the figure. The next subsections provide details about
the \mbox{RISC-V} CPU core and the Versat CGRA core.

\subsection{Adept}

Four \mbox{RISC-V} architectures have been studied in order to frame this work:
Taiga~\cite{matthews17}, a high performance \mbox{RISC-V} soft core; Rocket
Chip~\cite{Asanovic:EECS-2016-17}, the de facto standard used at UC Berkeley and
respective SiFive startup; PicoRV32~\cite{github-picorv32}, a size-optimized
architecture; and PULPino~\cite{gautschi17}. The proposed SoC in this work had a
very specific set of requirements for the General Purpose Processors (GPPs). The
\mbox{RISC-V} architecture must be small, use low power, and must target any
FPGAs and ASICs. Therefore, Rocket Chip was not selected because it had too many
features and proved too hard to manipulate. Taiga was not selected because it is
only optimized for FPGAs. Finally, PULPino and PicoRV32 were not selected
because they are written in conventional HDL. In this project an approach using
the abstractions present in modern programming languages is a requirement,
namely to facilitate parametrization of the RTL. Thus, after considering all
four architectures, it was decided to implement a new \mbox{RISC-V} architecture
from scratch using the RV32 ISA and the new Chisel
HDL~\cite{Bachrach:DAC2012}. The new architecture was called Adept.

There are two Adept configurations: one with lower performance and three
pipeline stages, which only implements the I instruction set (Adept-3), and
another with high performance and five pipeline stages, which implements the I
and M instruction sets (Adept-5). Both configurations were optimized for silicon
area, and only support bare metal applications. Both use instruction and data
caches of sizes 8 and 16kB, respectively. The Adept-3 block diagram is shown in
Fig.~\ref{fig_adept_3} and the Adept-5 block diagram of the in
Fig.~\ref{fig_adept_5}. The 3-Stage Adept \mbox{RISC-V} processor is open-source
under the Apache 2.0 License~\cite{github-adept}.

\begin{figure}[t]
\centering
\includegraphics[width=3.5in]{Adept}
\caption{Adept 3-Stage configuration top-level entity}
\label{fig_adept_3}
\end{figure}

\begin{figure}[t]
\centering
\includegraphics[width=3.5in]{Adept_5}
\caption{Adept 5-Stage configuration top-level entity}
\label{fig_adept_5}
\end{figure}

\subsection{Versat}

The Versat architecture is illustrated by means of its top-level diagram shown
in Fig.~\ref{fig_top_versat}. At the top of the diagram, one can see the Versat
Controller and its Program Memory (PM). The various modules in the system can be
accessed via the Control Bus, which is managed by the Controller. The Controller
executes programs stored in the PM (9kB = 1kB boot ROM + 8kB RAM).

A Versat program runs on the Controller and uses the Data Engine (DE) to carry
out data intensive computations. To perform these computations, the program
writes DE configurations to the Configuration Module (CM) or simply restores
configurations previously stored in the CM. The program can load the DE with
data to be processed or save the processed data back in the external memory
using the DMA engine. The DMA engine can also be used to initially load the
program in the PM or to move CGRA configurations between the core and the
external memory, if necessary.

\begin{figure}[t]
\centering
\includegraphics[width=3.5in]{top_versat}
\caption{Versat top-level entity.}
\label{fig_top_versat}
\end{figure}

The Versat core~\cite{Lopes16} has a host interface and a memory interface.  The
host interface (AXI slave) is used by a host system to instruct Versat to load
and execute programs. The host and Versat communicate using the Control Register
File (CRF). The CRF is also used by Versat programs as a general purpose
(1-cycle access) register file. The memory interface (AXI master) is used by the
DMA.

Different Versat instances can be generated containing different DE modules. The
DE consists of several Functional Units (FUs) interconnected in a full mesh. The
FUs are of 5 types:
\begin{enumerate}
\item dual-port embedded memory
\item feed-forward ALU
\item feedback ALU (can implement accumulators for various functions using an internal feedback loop)
\item multiplier
\item barrel shifter
\end{enumerate}

The interconnect mesh has 1 clock cycle of delay and the FUs themselves are
pipelined for better performance. The FUs are used to build hardware datapaths
that work on data streams. Example datapaths are illustrated in
Fig.~\ref{fig_de_dp}. Datapath (a) in the figure is a simple pipeline exploiting
Instruction-Level Parallelism (ILP) by performing {\it load, add} and {\it
  store} in parallel; datapath (b) demonstrates ILP plus Data-Level Parallelism
(DLP), or Thread-Level Parallelism (TLP) if the two additions are operated as
independent threads; datapath (c) is involves more FUs thus achieving more
parallelism.

\begin{figure}[t]
\centering
\includegraphics[width=3.5in]{datapaths}
\caption{Data engine datapaths.}
\label{fig_de_dp}
\end{figure}

The full mesh structure may seem an overkill but it greatly facilitates the
programming of Versat, dispensing with complex place and route algorithms. As
such, Versat can even be programmed in its assembly language, which is a unique
feature among CGRAs. Due to the relatively small number of FUs used, the full
mesh interconnect becomes affordable, occupying only 5\% of the chip
area~\cite{Lopes16}.


\section{Toolchain}
\label{sec:tchain}

\subsection{Adept}
The Adept \mbox{RISC-V} system has a fully operational toolchain for barebone
applications. Compilation, debugging and profiling are based on the
corresponding GNU tools. Most of the tools available for \mbox{RISC-V} are already
mature, and most have been merged upstream (their status is available
in~\cite{riscv-tools-status}).

%\subsubsection{Cross-compiler}
%\label{tchain:cross-compiler}

The main barebone cross-compiler used for \mbox{RISC-V} is GCC. The official GCC
distribution supports \mbox{RISC-V} as of version 7.1. There are options to make the
compilation aware of the floating-point unit, clock frequency for a specific
board, etc.

%Many cross-compilers available for commercial processors are built around GCC
%but include closed static libraries for many key functionalities, namely
%architectural simulation, IO, target loading and debugger connectivity. One
%might be tempted to develop clean room interface compatible processors, lured by
%the availability of GCC and other GNU tools. This is a mistake, as reproducing
%the closed components may be difficult, even though they represent a small part
%of the whole.


%\subsubsection{Debugger}

Debug support using GDB is already available for \mbox{RISC-V} cores but Adept does
not yet have hardware support for it. A debug module and corresponding JTAG
interface have not yet been incorporated.

When working with the Rocket Chip SoC Generator, the debug module and its
software support via OpenOCD~\cite{openocd} were not inter-operable in the
Warpbird setup~\cite{lf:rec18}, and therefore could not be integrated in
Adept. The Rocket Chip debug module and JTAG interface need to be debugged or
developed from scratch for Adept.

%\subsubsection{Chisel}

The Adept system is described in Chisel3, a novel hardware description
language~\cite{Bachrach:DAC2012}. A user can add components to the system using their preferred HDL. However, the
authors recommend using Chisel as it automates and eases tasks, such as
debugging, connections and interface generation.

%Chisel3 is built on top of Scala~\cite{scala}, a Java-like language with support
%for functional programming and a static type system. From a top down view Chisel
%is a Verilog macro engine, \textit{i.e.}, it generates a hardware description by
%composing pre-stored and small Verilog code snippets. Abstraction within Chisel
%is an important aspect of the language, as it allows the hardware description to
%be parameterizable, generic, and reusable.

%Chisel3 offers a testing environment itself described in Chisel and used to test
%designs. There are two simulation targets, a Chisel specific RTL simulator (the
%FIRRTL Interpreter~\cite{github-firrtl-interpreter}) or a Verilator target.

\subsection{Versat}

Being a CGRA, the Versat development tools are non-standard. Versat can be
programmed in assembly language~\cite{Lopes16} and using a C++
dialect~\cite{Santiago2017}.

Since the Versat Controller has access to the Data Engine (DE) and DMA engine by
means of its control bus, these modules can be programmed indirectly. To program
the DE, the datapaths are written to the Configuration Module (CM). To operate
the other modules, the Versat controller accesses their registers mapped in its
memory, as is usual when working with peripherals.


%\subsubsection{Assembler and compiler}

%To facilitate assembly programming, the assembler reads a dictionary containing
%easy to remember names for all registers in the memory map and useful Versat
%constants. Thus, 
For example, to configure ALU0 to add the outputs of multipliers MUL1 and MUL2,
one needs to store the constant ALU\_ADD to the ALU0\_FNS function select
configuration register, and the constants sMUL1 and sMUL2 to the configuration
registers ALU0\_selA and ALU0\_selB. This is made easy by Versat's full mesh
structure as the inputs of any FU can be connected to the output of any FU. If a
partial mesh were used, the programmer would have to remember what is connected
to what, making assembly programming not viable.

%Fortunately, most Versat programs are simple and can be written in assembly. For
%more complex programs, a high-level language that expedites the process of
%programming Versat with efficient datapaths was successfully implemented
%in~\cite{Santiago2017}. The Versat high-level programming language is a dialect
%of the C++ language.

\subsubsection{Debugger}

A debug tool for Versat has not yet been developed. Debugging a failing datapath
is sometimes difficult and a combination of techniques must be employed. The
Versat Controller is instrumental for debug as it provides control and
observability of the Data Engine. The controller can start the DE and program it
to stop at desired instants. It can also read the status of the DE registers
(every FU output is an accessible register) and any position of the DE embedded
memories.

Improving the debug flow would require the use of a standard debug tool for the
Versat Controller, such as GDB, which is planned for future work. In extreme
cases Versat is debugged using RTL simulation or FPGA Integrated Logic
Analyzers.


\section{Results}
\label{sec:impl_results}
\subsection{FPGA Implementation Results}
The Adept \mbox{RISC-V} processor and the Versat CGRA were both implemented for
Intel and Xilinx FPGA technologies. The FPGA implementation results are shown in
Tables~\ref{tab_fpga_adept_altera} and~\ref{tab_fpga_adept_xilinx},
respectively.

\begin{table}[t]
  \renewcommand{\arraystretch}{1.3}
  \caption{Adept FPGA implementation results for Arria V}
  \label{tab_fpga_adept_altera}
  \centering

  \begin{tabular}{|c||c|c|c|c|c|}
    \hline
    Architecture & Logic  & \#REGS  & RAM    & \#DSP & Fmax\\
                 & (ALMs) &       & (kbit) &       & (MHz)\\
    \hline
    \hline
    Adept-3      & 670   & 201  &  133   &   0   & 93  \\ % Cyclone V
    \hline
    Adept-5      & 1675  & 1470 &  266   &   4   & 120 \\ % Arria V
    \hline
  \end{tabular}
\end{table}

\begin{table}[t]
  \renewcommand{\arraystretch}{1.3}
  \caption{Adept FPGA implementation results for Kintex-7}
  \label{tab_fpga_adept_xilinx}
  \centering

  \begin{tabular}{|c||c|c|c|c|c|}
    \hline
    Architecture & Logic  & \#REG  & RAM    & \#DSP& Fmax\\
                 & (LUTs) &       & (kbit) &       & (MHz)\\
    \hline
    \hline
    Adept-3      & 1041  & 188   &  133    &  0    & 68 \\ % Zynq-7030-1
    \hline
    Adept-5      & 2533  & 1405  &  266    &  4    & 90 \\ % Kintex-7
    \hline
  \end{tabular}
\end{table}


As shown by these results, Adept-3 is a minimalistic \mbox{RISC-V}
implementation designed to become a programmable state machine in more complex
designs, and Adept-5 is a more standard CPU that can be used as a low complexity
host processor. Adept-3 needs to be better optimized for frequency, which, at
this development stage, was not a priority.

The critical path in the Adept 3-Stage configuration goes from the multiplexer
in the write-back stage, through the ALU, and ends in the branch execute block
of the instruction fetch stage. The critical path of the Adept-5 is in the
forwarding path from the memory stage to the end of the register fetch stage.

Versat results have been obtained for 4 different instances described in
Table~\ref{tab_versat_configs}, where \#ALU is the number of ALUs, \#MUL is the
number of 32x32=64-bit multipliers and \#BS is the number of barrel
shifters. The multipliers can be configured to output the lower or upper 32-bit
part of the 64-bit result. The FPGA implementation results are shown in
Tables~\ref{tab_fpga_versat_altera} and~\ref{tab_fpga_versat_xilinx}, for Intel
and Xilinx FPGAs, respectively.

\begin{table}[t]
  \renewcommand{\arraystretch}{1.3}
  \caption{Versat Instances}
  \label{tab_versat_configs}
  \centering
  
    \begin{tabular}{|c||c|c|c|c|}
    \hline
    Instance & \#ALU & \#MUL & \#BS    & \#MEM \\
    \hline
    \hline
    Versat-1  &   6    &    4    &   1    &   4    \\  % Versat
    \hline
    Versat-2  &  14    &    5    &   5    &   5    \\  % HE-AAC encoder / AC3 encoder
    \hline
    Versat-3  &   8    &    4    &   4    &   5    \\  % 
    \hline
    Versat-4  &   8    &    8    &   0    &   5    \\  % AC3 decoder, HE-AAC decoder
    \hline
  \end{tabular}
\end{table}

\begin{table}[t]
  \renewcommand{\arraystretch}{1.3}
  \caption{Versat FPGA implementation results for Arria V}
  \label{tab_fpga_versat_altera}
  \centering
  
    \begin{tabular}{|c||c|c|c|c|c|}
    \hline
    Instance & Logic & \#REG & RAM    & \#DSP & Fmax\\
             & (ALMs)  &      & (kbit) &       & (MHz)\\
    \hline
    % &  &  & (kbit) &  & Fmax(MHz)\\
    %\hline
    \hline
    Versat-1 &  8,607 & 4,673 & 351  & 32  & 130  \\  % Versat
    \hline
    Versat-2 & 15,340 & 12,620 & 623 & 25 & 120 \\  % HE-AAC encoder / AC3 encoder
    \hline
    Versat-3 & 13,700 & 11,270 & 623 & 21 & 120 \\  % HE-AAC decoder
    \hline
    Versat-4 & 10,680 & 11,200 & 623 & 36 & 120 \\  % AC3 decoder HE-AAC decoder
    \hline
  \end{tabular}
\end{table}

\begin{table}[t]
  \renewcommand{\arraystretch}{1.3}
  \caption{Versat FPGA implementation results for Kintex-7}
  \label{tab_fpga_versat_xilinx}
  \centering
  
   \begin{tabular}{|c||c|c|c|c|c|}
    \hline
    Instance & Logic    & \#REG & RAM    & \#DSP     & Fmax  \\
             &  (LUTs)  &       & (kbit) &           & (MHz) \\
    \hline
    % &  &  & (kbit) &  & Fmax(MHz)\\
    %\hline
    \hline
    Versat-1 & 12,510 & 4,396  & 360 & 16 & 102 \\ % Versat
    \hline
    Versat-2 & 23,750 & 13,158 & 630 & 25 & 90  \\  % HE-AAC encoder / AC3 encoder
    \hline
    Versat-3 & 21,184 & 11,740 & 630 & 21 & 90  \\  % 
    \hline
    Versat-4 & 16,647 & 11,699 & 630 & 36 & 90  \\  % AC3 decoder, HE-AAC decoder
    \hline
  \end{tabular}
  
\end{table}

The results show that Versat cores can be implemented in different sizes with
minimal impact on the frequency of operation. However, due to the full mesh
structure, the frequency does not increase above 130MHz, which is the result
obtained for the smallest of the instances.

Overall, systems comprising several Adept and Versat instances can be
implemented with reasonable hardware resources. When implemented in an FPGA, the
present platform is an overlay architecture providing fast reconfiguration of
the Versat core in exchange for the overhead of not implementing applications
directly on the FPGA fabric, which can only be reconfigured very slowly.

\subsection{ASIC Implementation Results}

Versat and Adept have been implemented in the UMC 130nm process. Versat
has been further implemented in the TSMC 65nm process. 

Tables~\ref{tab_asic_r_adept} and~\ref{tab_asic_r_versat} show the results, for
Adept and Versat, respectively, in terms of the technology node (N), silicon
area (A), embedded memory (M), frequency of operation (F) and power consumption
(P). The frequency and power results have been obtained using the Cadence IC
design and simulation tools. The power figure has been obtained using the same
tools and the node activity rate extracted from simulation.

Results have been extrapolated (not measured) to 28nm and 40nm nodes to
illustrate how they vary with the technology node using the rules explained
in~\cite{Huang2011}.

\begin{table}[t]
  \renewcommand{\arraystretch}{1.3}
  \caption{Adept Integrated circuit implementation results.}
  \label{tab_asic_r_adept}
  \centering
  \begin{tabular}{|c|c|c|c|c|c|}
    \hline
    Core       & N(nm) & A(mm\textsuperscript{2}) & M(kB) &  F(MHz) & P(mW)\\
    \hline
    \hline
    Adept-3    & 130   & 1.61     & 16  & 200     & 48 \\
    \hline
    Adept-3    & 40    & 0.15     & 16  & 650     & 15  \\
    \hline
    Adept-5    & 65    & 0.92     & 32  & 420     & 58 \\
    \hline
    Adept-5    & 40    & 0.35     & 32  & 683     & 36 \\
    \hline
    Adept-5    & 28    & 0.17     & 32  & 975     & 25  \\
    \hline
  \end{tabular}
\end{table}

\begin{table}[t]
  \renewcommand{\arraystretch}{1.3}
  \caption{Versat Integrated circuit implementation results.}
  \label{tab_asic_r_versat}
  \centering
  \begin{tabular}{|c|c|c|c|c|c|}
    \hline
    Instance            & N(nm) & A(mm\textsuperscript{2}) & M(kB) &  F(MHz) & P(mW)\\
    \hline
    \hline
    Versat-1            & 130   & 5.20  & 46    & 170     &  132 \\
    \hline
    Versat-1            & 40    & 0.49 & 46    & 553     &  41  \\
    \hline
    Versat-2            & 65    & 2.38 & 76    & 500     &  179 \\
    \hline
    Versat-2            & 40    & 0.90 & 76    & 813     &  110  \\
    \hline
    Versat-2            & 28    & 0.44 & 76    & 1161    &  77  \\
    \hline
    Versat-3            & 65    & 2.18 & 76    & 510     &  167 \\
    \hline
    Versat-3            & 40    & 0.83 & 76    & 829     &  102 \\
    \hline
    Versat-3            & 28    & 0.41 & 76    & 1184    &  72  \\
    \hline
    Versat-4            & 65    & 2.10 & 68    & 510     &  160 \\
    \hline
    Versat-4            & 40    & 0.70 & 68    & 829     &  86 \\
    \hline
    Versat-4            & 28    & 0.34 & 68    & 1184    &  60  \\
    \hline
  \end{tabular}
\end{table}

The results show that the proposed heterogeneous computing platform is
competitive regarding silicon area and power consumption. A platform consisting
of a few Adept and Versat cores consumes a few hundred milliwatt in a 40nm
process, while a single ARM Cortex A9 core can consume about half a watt and
occupy a larger silicon area~\cite{Lopes16}. However, a more relevant comparison
by running actual applications is in order.

\subsubsection{Applications}

The present heterogenous computing platform targets applications for low cost
battery operated devices. These applications can benefit from the low energy
features of the CGRAs. The applications that have been tested in the present
architecture are outlined in Table~\ref{tab_apps}.

\begin{table}[t]
  \renewcommand{\arraystretch}{1.3}
  \caption{Audio encoders and decoders}
  \label{tab_apps}
  \centering

  \begin{tabular}{|c||c|c|}
    \hline
    Audio Format & \#CPUs & \#CGRAs \\
    \hline
    \hline
    MPEG1/2 Layers I/II encoder/decoder (2 chs) & 1 & 0 \\
    \hline
    AAC-LC stereo/multichannel decoder (6 chs)  & 1 & 0 \\
    \hline
    AAC-LC stereo encoder (2 chs) & 1           & 0 \\
    \hline
    AAC-LC multichannel encoder (6 chs)         & 3 & 0 \\
    \hline
    HE-AAC stereo encoder/decoder (2 chs)       & 1 & 1 \\
    \hline
    HE-AAC multichannel encoder/decoder (6 chs) & 3 & 1 \\
    \hline
    AC3 stereo encoder/decoder (2 chs)          & 1 & 1 \\
    \hline
    AC3 multichannel encoder/decoder (6 chs)    & 3 & 1 \\
    \hline
     K-Means Clustering                         & 0 & 1 \\
    \hline
  \end{tabular}
\end{table}


The systems created for some of the audio applications have been compared with
an ARM Cortex-A9 processor running the same applications. Previously, the
multi-dimensional K-Means algorithm was shown to run 3.8x faster while consuming
46.3x less energy running on Versat compared to running on the ARM
Cortex-A9~\cite{Lopes2017-fpl}. The results obtained for a demanding audio codec
application using a 40nm process are shown in Table~\ref{tab_apps_results}.

\begin{table}[t]
  \renewcommand{\arraystretch}{1.3}
  \caption{Implementation Comparison between the proposed system and an ARM Cortex-A9 for HE-AAC 5.1 audio applications}
  \label{tab_apps_results}
  \centering

  \begin{tabular}{|c||c|c|c|c|}
    \hline
                                    & Area                   & RAM  & Freq. & Power   \\
    Core (40nm) / Software          & (mm\textsuperscript{2})& (kB) & (MHz) & (mW)    \\
    \hline
    \hline
    ARM Cortex-A9 / Decoder         & 4.6                    & 64   & 50    & 31.25   \\
    \hline
    ARM Cortex-A9 / Encoder           & 4.6                    & 64   & 70    & 43.75   \\
    \hline
    \hline
    3xAdept-5 + Versat-4 / Decoder    & 1.75                   & 164  & 80    & 20.76   \\
    \hline
    3xAdept-5 + Versat-2 / Encoder    & 1.95                   & 172  & 80    & 23.31   \\
    \hline
  \end{tabular}
\end{table}

For the HE-AAC 5.1 audio decoder, the ARM
Cortex-A9~\cite{wang,arm-audio-presser} is 2.6x larger than the proposed
implementation and consumes 1.5x more power.  For the HE-AAC 5.1 audio encoder,
the ARM Cortex-A9~\cite{wang,arm-audio-presser} is 2.4x larger than the proposed
implementation and consumes 1.9x more power.

It should be noted that the Versat instances for running the audio encoders
could have been much better optimized if more engineering resources had been
available. Furthermore, the ARM results have benefited from the use of its NEON
SIMD unit optimized at the assembly level. The research team unsuccessfully
attempted to reproduce these results using solely compiler directives for
vectorization using NEON. It was further observed that writing some functions,
which use the NEON unit, in assembly language could yield 2-3x speedup compared
to using compiler directives. Therefore, the obtained results are promising for
reducing the device cost (which varies exponentially with the silicon area) and
the power consumption, leading to more energy efficient solutions.

\section{Conclusion}

In this work a low power heterogeneous system is proposed, composed of
\mbox{RISC-V} processors and Versat CGRA cores, targeting any design flow (FPGA
or ASIC). The designed \mbox{RISC-V} core, Adept, has two configurations: a
small 3-stage pipeline configuration, and a more performant 5-stage pipeline
configuration. In both instances, Adept possesses a competitive energy footprint
and resource usage. The Versat CGRA allows for a large set of applications to be
executed with minimal energy requirements and high performance.

The free \mbox{RISC-V} ISA enables the development of SoCs that use one or more
processor instances, without the need to pay any license fees or royalties.
Moreover, the \mbox{RISC-V} ISA is supported by a rich open source toolchain and
a thriving developer community. In this work, it was concluded that the RTL
still needs to be developed in-house as the available open source projects are
still difficult to manipulate and extend.

When implemented in FPGA, the present platform constitutes an overlay
architecture. Applications run slower on it compared to when directly mapped to
the FPGA fabric. However, using fast on-the-fly reconfiguration, Versat is able
to multiplex large virtual hardware circuits into a small CGRA, and run
applications that otherwise would not fit in the FPGA. The implementation
results show that it is possible to instantiate several \mbox{RISC-V} and Versat
cores in mid-range FPGAs and build useful overlay architectures which are
capable of running real-world applications such as the audio encoders and
decoders presented herein. When implemented as an ASIC, the proposed
heterogeneous computing platform consumes close to 3x less silicon area and 2x
less energy compared to an ARM Cortex-A9 equipped with the NEON SIMD unit and
running the same applications. One particular application, the big data K-means
Clustering algorithm, had previously been shown to run 3.8x faster while
consuming 46.3x less energy compared to the same ARM
processor~\cite{Lopes2017-fpl}.


% use section* for acknowledgment
\section*{Acknowledgment}

This work was supported by national funds through Funda\c c\~ao para a Ci\^encia
e a Tecnologia (FCT) with reference UID/CEC/50021/2013.

% Can use something like this to put references on a page
% by themselves when using endfloat and the captionsoff option.
\ifCLASSOPTIONcaptionsoff
  \newpage
\fi

% trigger a \newpage just before the given reference
% number - used to balance the columns on the last page
% adjust value as needed - may need to be readjusted if
% the document is modified later
%\IEEEtriggeratref{8}
% The "triggered" command can be changed if desired:
%\IEEEtriggercmd{\enlargethispage{-5in}}

% references section

% can use a bibliography generated by BibTeX as a .bbl file
% BibTeX documentation can be easily obtained at:
% http://mirror.ctan.org/biblio/bibtex/contrib/doc/
% The IEEEtran BibTeX style support page is at:
% http://www.michaelshell.org/tex/ieeetran/bibtex/
%\bibliographystyle{IEEEtran}
% argument is your BibTeX string definitions and bibliography database(s)
%\bibliography{IEEEabrv,../bib/paper}
%
% <OR> manually copy in the resultant .bbl file
% set second argument of \begin to the number of references
% (used to reserve space for the reference number labels box)

\bibliographystyle{unsrt}
\bibliography{./bibtex/BIBfile}

% biography section
% 
% If you have an EPS/PDF photo (graphicx package needed) extra braces are
% needed around the contents of the optional argument to biography to prevent
% the LaTeX parser from getting confused when it sees the complicated
% \includegraphics command within an optional argument. (You could create
% your own custom macro containing the \includegraphics command to make things
% simpler here.)
%\begin{IEEEbiography}[{\includegraphics[width=1in,height=1.25in,clip,keepaspectratio]{mshell}}]{Michael Shell}
% or if you just want to reserve a space for a photo:


% that's all folks
\end{document}



%%  LocalWords:  parametrization
