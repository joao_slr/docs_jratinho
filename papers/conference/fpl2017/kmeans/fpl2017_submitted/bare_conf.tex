\documentclass[conference,a4paper]{IEEEtran}
\usepackage{amsmath}
\usepackage[pdftex]{graphicx}
% declare the path(s) where your graphic files are
\graphicspath{{./drawings/}}
% and their extensions so you won't have to specify these with
% every instance of \includegraphics
\DeclareGraphicsExtensions{.pdf,.jpeg,.png,.eps}

% correct bad hyphenation here
\hyphenation{}

\begin{document}
%
% paper title
% can use linebreaks \\ within to get better formatting as desired
\title{K-means clustering on CGRA}


% author names and affiliations
% use a multiple column layout for up to three different
% affiliations
%\author{
%\IEEEauthorblockN{Jo\~ao D. Lopes, Jos\'e T. de Sousa, Hor\'acio Neto}
%\IEEEauthorblockA{INESC-ID Lisboa / T\'ecnico Lisboa\\
%University of Lisbon\\
%Emails: joao.d.lopes@ist.utl.pt, jts@inesc-id.pt, hcn@inesc-id.pt}
%\and
%\IEEEauthorblockN{M\'ario V\'estias}
%\IEEEauthorblockA{INESC-ID Lisboa / ISEL\\
%Instituto Polit\'ecnico de Lisboa \\
%Email: mvestias@deetc.isel.pt}
%}

% make the title area
\maketitle


\begin{abstract}
%\boldmath
In this paper we present a k-means clustering algorithm for the Versat
architecture, a small and low power Coarse Grained Reconfigurable
Array (CGRA). This approach targets ultra low energy applications such
as those found in wireless sensor networks, where using a GPU or FPGA
accelerator is out of the question. The algorithm uses 32-bit integer
precision and the Manhattan norm. The Versat architecture has been
enhanced with pointer support, the possibility of using the address
generators for general purposes, and a new ALU type with cumulative
and conditional operations. The Versat program creates two base
hardware datapaths for the two basic steps of the algorithm: the
assignment and the update steps. These datapaths are partially
reconfigured for each processed datapoint. The program has been
written in assembly and has 691 instructions. It is fully
parameterizable with the number of datapoints, centroids, coordinates,
and memory pointers for reading and writing the data. The execution
time scales linearly with the number of datapoints, centers or
dimensions. The results show that the new Versat core is 9.4x smaller
than an ARM Cortex A9 core, and runs the algorithm 3.8x faster while
consuming 46.3x less energy.

\end{abstract}

\IEEEpeerreviewmaketitle


\section{Introduction}

Clustering consists in grouping a set of similar objects, according to
some metric, in the same cluster. It is one of the most useful and
used tasks in exploratory data mining, and can be applied in a wide
variety of fields. Being an unsupervised learning method, previous
training with a training set is not required, and classification can
be performed directly from the unclassified dataset. Clustering can be
a time consuming process, requiring several iterations through the
dataset, especially if the dataset is large in both the number of
points and dimensions. The increasing scientific interest in Big Data
(i.e., very large datasets) and real-time clustering emphasize the
need for producing clustering results fast. This problem can be
attenuated by the use of hardware accelerators.

One of the most widely used clustering algorithms is the k-means
clustering algorithm, addressed in this paper. The acceleration of the
k-means clustering algorithm has been a hot topic in the scientific
community for quite some time. Its importance has been increasing
dramatically with the demand for efficiently classifying large
datasets.

K-means clustering has been accelerated using Graphics Processor Units
(GPUs)~\cite{Che07,farivar2008} and more recently using Field
Programmable Gate Arrays
(FPGAs)~\cite{Liu2005,hussain2011,kutty2013,canilho2016}. Although
these implementations have been shown to improve performance and
energy consumption over General Purpose Processor (GPP)
implementations, they can hardly be used in used in ultra low energy
scenarios, such as in Wireless Sensor Networks (WSNs). K-means
clustering is often employed in WSNs to group nodes based on their
position and energy. These clusters are responsible for collecting and
treating information locally, transmitting only relevant parameters to
the base station. In~\cite{Sasikumar2012}, it has been demonstrated
that it is more efficient, in terms of time and energy, to run the
k-means clustering algorithm in a distributed fashion rather than in a
centralized way. This is because one avoids sending/receiving large
amounts of data to/from a central node or base station. Since WSN
nodes are extremely constrained in terms of computing power and
energy, a smaller and more power efficient accelerator should be used,
such as a Coarse Grained Reconfigurable Array (CGRA). A dedicated
hardware accelerator may also be used but its lack of programmability
can become a serious liability in the long run.

To the best of our knowledge, a detailed description of a k-means
clustering algorithm for a CGRA cannot be found in the literature,
although instances of the algorithm have been used in benchmark
sets~\cite{Essen2009}. This paper presents an in-depth study of a
fully parameterizable implementation of the k-means clustering
algorithm on a previously published CGRA -- the Versat
architecture~\cite{Lopes16}. The Versat CGRA features a relatively low
number of functional units, achieving a very small area footprint and
low energy consumption. Reconfiguration is quasi-static (occurring
only after complete loop nests are run) and partial (only the
configuration words that differ from the previous configuration are
changed). Unlike other CGRAs, which are programmed by pre-compiling
configurations for their programmable hardware, Versat is programmed
by programming its very small 16-instruction controller, which
generates and/or modifies hardware configurations in runtime. The
Versat controller can be programmed in assembly language or using a
small C++ subset for which a compiler has been described
in~\cite{Santiago2017}. The compiler could not be used in the present
work, as new hardware features have been added to the previous Versat
architecture, and used in the present k-means clustering
implementation. Therefore, the software has been written in
assembly. It should be noted that the new features are general and
useful in other algorithms.

The proposed algorithm compares datapoint and centroid coordinates
sequentially and mostly exploits Instruction-Level Parallelism
(ILP). It also updates the position of the centroids sequentially by
accumulating coordinates of datapoints in the same cluster and
dividing the accumulated values by the cluster size. This way, the
architecture is highly scalable, with constant acceleration compared to a
GPP. The Manhattan norm is adopted as the distance metric for
32-bit fixed-point data. Adopting the Euclidean norm is difficult
without floating point support. More exotic and non constant
norms have not been considered so far.

The remainder of the paper is organized as
follows. Section~\ref{sec:kmeans} describes the k-means clustering
algorithm. Section~\ref{sec:versat} briefly outlines the Versat
architecture. Section~\ref{sec:kernel} presents the proposed
algorithm. Section~\ref{sec:results} presents implementation and
performance results. Section~\ref{sec:conc} concludes the paper.


\section{K-means clustering}
\label{sec:kmeans}

The k-means algorithm is one of the simplest algorithms for performing
the clustering task. Despite its simplicity, it is still one of the
most widely used clustering algorithms, due to its ease of
implementation and fast execution time. The algorithm uses a centroid
model. It separates the data into a set of clusters, each having a
centroid represented by the mean vector of all the data points in the
cluster. Each data point is classified as being in the cluster whose
centroid is closest to it. The Euclidean distance is a common metric,
though other types of metrics can be applied~\cite{Estlick2001}. After
an initial position is given to each centroid, the algorithm starts
updating the position of the centers in an iterative fashion. Each
iteration is divided in two main steps:
\begin{enumerate}
\item Assignment step: each datapoint is assigned to the nearest
  centroid, given the chosen distance metric
\item Update step: the centroids are recalculated; the new positions
  correspond to the mean of all the data points in each cluster
\end{enumerate}
The algorithm ends when the centers are unchanged after an iteration.


\section{Versat architecture}
\label{sec:versat}

The Versat architecture is presented in detail
in~\cite{Lopes16}. Here, only a brief description is presented for
self containability reasons. The top level entity is shown in
figure~\ref{fig:top}. Referring to this figure, the Controller
executes the program stored in the 2048x32bit Program Memory (PM). The
communication between a host system and Versat is done through the
Control Register File (CRF), consisting of 16 registers, R0 through
R15. The host uses the CRF to pass parameters to Versat programs,
initiate their execution and check if they have finished. The Data
Engine (DE) is where most computations take place. The controller
writes new DE configurations to the Configuration Module (CM). These
configurations can be selected for execution and/or partially
modified. The Direct Memory Access (DMA) module is used to access the
external memory and transfer data/instructions/configurations to/from
the DE/PM/CM, respectively. The DE receives commands from the
controller and informs the controller of its status. The DE is run
many times during the execution of a program, for many different DE
configurations.

\begin{figure}[!htb]
  \centering
  \includegraphics[width=3.5in]{top.png}
  \caption{Versat top-level entity.}
  \label{fig:top}
\end{figure}

\subsection{Controller}

The Versat controller runs the kernel algorithms and is responsible
for the overall control of the system, especially the reconfiguration
process. It performs loads and stores (from/to the Control Bus), a few
arithmetic and logic operations, and branch instructions. The
controller has not been designed for high performance. Its instruction
set consists of just 16 instructions and its performance is just
enough for controlling the main steps of the algorithms, reconfiguring
the DE and operating the DMA; data intensive computations are done in
the DE.

\subsection{Data Engine}
\label{sec:de}

The Data Engine (DE) is a 32-bit architecture and comprises 15
functional units (FUs) as shown in figure~\ref{fig:de}. There are four
2048x32bit dual port embedded memories, six ALUs, four multipliers and
one barrel shifter. The outputs of all FUs are concatenated to form a
19x32-bit Data Bus. (Since each memory contributes two outputs, there
are {\bf 2}*4+6+4+1=19 bus sections.) Each FU input can be configured
to select any of the 19 sections of the bus -- this implements a full
mesh topology and avoids contention on the Data Bus.

\begin{figure}[!htb]
  \centering
  \includegraphics[width=3.5in]{de.png}
  \caption{Versat data engine.}  
  \label{fig:de}
\end{figure}

The embedded memories have an Address Generator Unit (AGU) for each
port. The AGU can support two-level {\tt for} loop nests. It is
possible to configure and run each AGU independently. The AGU
parameters are described in~table~\ref{tab:MemParameter}. Two new AGU
features have been added for this work: (1) by configuration, the AGU
can be bypassed and the memory port address is input by the other
port; (2) the number sequences generated by an AGU can be output to
the DE for general purposes, instead of being used as addresses. These
new AGU features are not specific to the k-means algorithm and have
generic use. What feature (1) actually does is to provide Versat with
the capability of working with pointers, as these can be computed in
the DE and routed to any embedded memory to be used as
addresses. Feature (2) exploits the ability of the AGUs to generate
synchronization sequences, making it possible to use them to
synchronize FUs in the DE. The two ports can be independently
configured to read and/or write the memories. The write configuration
includes the selection of a data bus section to be written in the
memory.
\begin{table}[h!]
    \caption{Address generator unit parameters.}
  \begin{center}
    \begin{tabular}{|l|c|p{5.5 cm}|}
      \hline
      {\bf Parameter} & {\bf Size} (bits) & {\bf Description} \\
      \hline \hline
      Start & 11 & Memory start address. Default value is 0. \\
      \hline
      Per  & 5 & Number of iterations of the inner loop, aka Period. Default is 1 (no inner loop). \\
      \hline
      Duty & 5 & Number of cycles in a period (Per) that the memory is enabled. Default is 1.\\
      \hline
      Incr & 11 & Increment for the inner loop. Default is 0.\\
      \hline
      Iter & 11 & Number of iterations of the outer loop. Default is 1.\\
      \hline
      Shift & 11 & Additional increment in the end of each period. Note that Per+Shift is the increment of the outer loop. Default is 0.\\
      \hline
      Delay & 5 & Number of clock cycles that the AGU must wait before starting to work. Used to compensate different latencies in the converging branches of the configured hardware datapaths. Default is 0.\\
      \hline
      Reverse & 1 & Bit-wise reversion of the generated address. Certain applications like the FFT work with mirrored addresses. Default is 0.\\
      \hline
    \end{tabular}
  \end{center}
  \label{tab:MemParameter}
\end{table}

There are two types of ALUs, which differ in the supported
operations. Two ALUs are of type I, supporting basic arithmetic and
logic operations, while four ALUs are of type II, supporting basic
operations plus the same operations using as operands one of the
inputs and the ALU output itself, while the other input is used for
conditional control. ALUs of type II is a new Versat feature
introduced in this paper. Again it should be stated that these
features are general and can be used in other algorithms. What has
been done was to add internal feedback and conditional execution to
the type II ALUs. For example, the basic operation of {\em addition}
can be turned into a conditional accumulate operation: the ALU can
register or accumulate the value present on one of its inputs,
depending on the value present on the other input. In another example,
the {\em minimum} operation can be used to detect and register the
minimum value of a sequence present in one of the ALU inputs, while
the other input qualifies the value present at the data input. The new
AGUs can be used to generate control sequences for the conditional ALU
input: the computation of the minimum and registration of the result
at the ALU output will happen only in certain time instants; at other
instants the last computed minimum is kept.

The multipliers take two 32-bit operands and produce a 64-bit result,
of which only 32 bits are used. It is possible to choose the upper or
lower 32-bit part of the result. It is also possible multiply the
output by two, which is useful to keep the Q1.31 fixed-point format,
used in many DSP algorithms. For the barrel shifter, one operand is
the word to be shifted and the other operand is the shift size. The
barrel shifter is configured with the shift direction (left or right)
and the right shift type (arithmetic or logic).

Each FU has a latency due to pipelining: 2 cycles for the ALU, 3
cycles for the multiplier and 1 cycle for the barrel shifter. Thus,
when configuring a datapath in the DE, it is necessary to take into
account the latency of each branch, and compensate for any mismatches
when branches with different latencies converge. To do this, the AGUs
have the {\em Delay} parameter explained in
table~\ref{tab:MemParameter}. The branch latency is the sum of its FU
latencies.

To control the DE, it is necessary to write ​​to its control
register. It is possible to initialize/run the FUs by setting bits 0/1
of the control register, respectively. Bits 2-20 select the FUs to
initialize or run. Since each memory port can be controlled
independently, there are {\bf 2}*4+11=19 bits in the DE control
register for selecting the FUs. To check the DE, its status register
is used. The status register indicates which AGUs have ended execution
(bits~0~to~7).


\subsection{Configuration module}

The configurations of the DE are stored in the Configuration Module
(CM). The CM comprises the configuration register file, the
configuration shadow register and the configuration memory. The
configuration register file is used by the Versat controller to write
configuration words to the DE (partial reconfiguration). The shadow
register allows maintaining the currently active DE configuration
while a future configuration is being created in the configuration
register. The configuration memory can be used to save 64 frequently
used DE configurations. A configuration in the configuration register
file can be saved in the configuration memory in 1 clock cycle and
reloaded later for reuse in another clock cycle.

\subsection{DMA}
\label{sec:DMAengine}

The Versat controller uses the DMA module to access an external memory
and transfer programs, data and configurations. The maximum block
transfer size is 256 words of 32 bits. The DMA can work in parallel
with the controller and the DE.


\section{The k-means clustering kernel}
\label{sec:kernel}

The k-means clustering kernel follows the algorithm given in
section~\ref{sec:kmeans}, creating and partially reconfiguring the two
basic hardware datapaths that realize the {\em assignment} and {\em
  update} steps of the algorithm. The kernel is written in the Versat
assembly language and it is 691 instructions long.

First, the two datapaths are created and stored in the configuration
memory. This is done while the first chunk of data is being
transferred into the DE by the DMA. After reading the number of
points, number of coordinates, number of centroids and respective
external memory addresses from the CRF, as passed by the host, the
program instructs the DMA to load the first datapoint chunk and
initial centroid values in memories MEM0 and MEM1, respectively. The
datapoint chunk size cannot exceed 2048x32bits, the size of MEM0. The
number of centroids times the number of coordinates is limited to
1024x32bits, half the size of MEM1. The other half is reserved for
incrementally building the updated centroids. This is a hard
limitation of this algorithm, which can only be overcome by using a
larger embedded memory for the centroids, which costs silicon, or by
streaming the centroids as done with the datapoints, which penalizes
performance. However, in the target WSN applications, only a few
centroids are used, making the current algorithm acceptable.

\begin{figure}[!htb]
  \centering
  \includegraphics[width=3.5in]{kmeans.png}
  \caption{Datapath for the assignment step.}  
  \label{fig:assignment}
\end{figure}

The datapath in figure~\ref{fig:assignment} implements the assignment
step. Its configuration is loaded from the configuration memory into
the configuration register in one clock cycle, and shifted to the
shadow configuration register in another cycle. The DE is started and
the first point is compared with all the centroids, coordinate after
coordinate. The absolute value of the coordinate difference is
accumulated in the type II ALU, configured with the {\em ACC if A}
function. In this function, the A input instructs the ALU to either
store the first absolute difference or accumulate the following
ones. Input A is produced by the address generator B of MEM1, which is
thus not used to generate memory addresses -- this is a new Versat
feature as explained in section~\ref{sec:de}. If $A < 0$ then the ALU
stores, otherwise it accumulates. After processing all coordinates,
the Manhattan Distance (MD) is ready. The ALU configured as {\em MIN
  if A} checks whether the current centroid is the closest yet to the
datapoint, by computing the minimum between the current MD and the
previous minimum. Input A is generated by port B of MEM2 to
synchronize this action. The current MD is delayed by 1 cycle using
the barrel shifter configured as {\em DELAY 1} (i.e., no shift), so
that it can be compared with the current minimum. If it is smaller,
then it will be the next minimum, and the centroid index, generated by
port B of MEM0, is stored in the ALU configured as {\em REGISTER},
when signaled by port A of MEM2, which tells when is the new minimum
valid. The logic AND between the existence of a new minimum and the
moment when it is valid is implemented by a multiplier, since all the
6 ALUs have been used up. Note that port B of MEM0 and both ports of
MEM2 need to delay their start instant considerably, waiting for all
coordinates or all centroids to be processed, plus the datapath
latency. This is not possible to achieve with the 5-bit {\em Delay}
AGU parameter given in table~\ref{tab:MemParameter}, so it is the
controller that starts these AGUs at the right time. Finally, the
point label (closest centroid index) is stored in MEM3 after its port
A receives a signal from the controller at the precise timing after
all centroids are compared to the datapoint.

To process the next datapoint, the DE is partially reconfigured to
advance the addresses of the datapoint and its label in ports A of
MEM0 and MEM3, respectively. It is not possible to advance the
datapoint without reconfiguration, as this would require our AGUs to
support 3 levels of nested loops when they only support two. Their 2
levels are used to go through all centroids ({\em Iter} = number of
centroids) and all coordinates for each centroid ({\em Per}=number of
coordinates). Also note that we have {\em Shift=-Per} for the AGU of
port A of MEM0, so that its generated address goes back to the first
coordinate after the last coordinate. The partial reconfiguration time
for the next datapoint is hidden, as it occurs while the DE is running
the current datapoint. This process repeats until all datapoints in
the first chunk are processed.

\begin{figure}[!htb]
  \centering
  \includegraphics[width=3.5in]{kmeans-acc.png}
  \caption{Datapath for the update step.}  
  \label{fig:update}
\end{figure}

After the assignment step is done, the datapath for performing the
update step, shown in figure~\ref{fig:update}, is loaded. In this
datapath, the point coordinates, already loaded in the previous
datapath, are read from port B of MEM0. The point labels (centroid
indices), computed in the previous step, are read from port A of
MEM3. Its port B is generating coordinate offsets synchronized with
the datapoint coordinates. An ALU adds labels and coordinate offsets
to produce the accumulated coordinate address of the centroid to
update, which is fed to port A of MEM1. By configuration, port A can
serve as the address for port B, another of Versat's new features
explained in section~\ref{sec:de}. Thus, the updated centroid
accumulated coordinate is read from port B, added to the current point
coordinate and stored back to port B at the same address. Each port
has an input and an output and can read an old value and write a new
value to the same address. A similar loop is used for updating the
number of occurrences of each centroid in MEM2. The number of
occurrences for each centroid is stored in MEM2 and is incremented
each time it is addressed, using the same address fed to port A of
MEM1. The incrementer is implemented by another ALU. Note that any FU
can select as input the commonly used constants 0 and 1, instead of a
data bus entry. The two memory read-add-write self loops take 4 clock
cycles due to the accumulated latencies of these
operations. Therefore, the inner loop of the AGUs has size 4
($Per=4$), and the outer loop size equals the number of coordinates
($Iter=$ number of coordinates). Also note that port B of MEM1 and
MEM2 are only enabled for writing in the last cycle of the 4-cycle
inner loop ($Duty=1$). After this nested loop runs, the DE is
partially reconfigured to point to the next datapoint, as is done in
the assignment step datapath. This process repeats until all
datapoints in the first chunk are processed.

After the assignment and update steps are run, the next chunk of
datapoints is loaded in MEM0 and the two steps are applied to the new
chunk. This process is repeated until all data points are processed.

After all datapoint chunks are processed, the updated centroid
accumulated coordinates reside in MEM1 and must be divided by the
number of occurrences in MEM2 to yield the new centroid
coordinates. Since the DE has no divider, a fixed-point serial divider
has been added as a Controller peripheral, taking 33 cycles per
division. While the divider is running, there is time to check whether
the newly computed centroid coordinate differs from the one stored in
MEM1, overwriting it if it does. If none of the new centroids is
different from the stored centroids, the algorithm terminates and the
final centroids are DMA transferred to the external memory. If the
point labels are also needed as a final result, then the assignment
step must be run again for all datapoints, as the labels for each data
chunk are not kept. This final loop represents a small overhead if the
algorithm has many iterations. The labels produced during this extra
iteration are DMA transferred to the external memory for each
datapoint chunk.

Finally, the program clears register R0 in the CRF, which signals the
host that Versat has finished execution.

\section{Results}
\label{sec:results}

In this section, design and performance results are presented. Versat
has been designed as an IP core, which can be integrated in a System
on Chip (SoC). Results on core area, frequency and power are
given. Versat's performance for the presented k-means clustering
algorithm is measured and compared to a widely used embedded
processor: the ARM Cortex A9 processor.

\subsection{IP core implementation results}

Versat has been designed using a UMC 130nm process and its main
features are shown in table~\ref{tab_asic_r}: technology node (N),
silicon area (A), embedded memory (RAM), frequency of operation (F)
and power (P). For the sake of comparison we also show the features of
a previous Versat version and of the ARM Cortex A9 processor,
implemented in a 40nm technology and optimized for
power~\cite{wang}.

The Versat frequency and power results for the 130nm process have been
obtained using the Cadence IC design and simulation tools. The
frequency is reported by the place and route tool, which produces a
netlist with back-annotated layout information. Power is obtained by
simulating this netlist to get node activities, and then computing a
power figure. For synthesis, the Encounter RTL Compiler tool (v11.20)
has been used. For place and route, the Encounter Design
Implementation tool (v11.10) has been used. For simulation, the NCSIM
simulator (v12.10) has been used. To better compare with the ARM
processor, the last row in table~\ref{tab_asic_r} shows the Versat
features scaled to a 40nm technology. These results have been obtained
by applying scaling rules as explained in~\cite{borkar99}.

\begin{table}[h]
\caption{Integrated circuit implementation results}
\label{tab_asic_r}
\centering
\begin{tabular}{|c|c|c|c|c|c|}
\hline
Core & N(nm) & A(mm\textsuperscript{2}) & RAM(kB) &  F(MHz) & P(mW)\\
\hline
\hline
Versat~\cite{Lopes16} & 130 & 4.2  &  46.34 & 170 &  99 \\
\hline
Versat~[here] & 130 & 5.2  &  46.34 & 170 &  132 \\
\hline
ARM Cortex A9~\cite{wang} & 40 & 4.6 &  65.54 & 800 &  500 \\
\hline
Versat [here, scaled] & 40 & 0.49  &  46.34 & 553 &  41 \\
\hline
\end{tabular}
\end{table}

From the above results, we conclude that the current Versat core is
9.4x smaller than the ARM core and consumes 12.2x less power, assuming
both cores are clocked at their work frequencies. The Versat power has
been obtained by simulating the present k-means clustering
algorithm. The results show that the current Versat version is 24\%
larger than the previous version presented in~\cite{Lopes16}, and
consumes 32\% more power, discounting the fact that in~\cite{Lopes16}
power was measured for an FFT kernel. The ARM power figure is an
average value reported in~\cite{wang}.

\subsection{Performance results}

To study our solution's performance we used a high number of randomly
generated datasets, varying the number of data points, centroids and
coordinates. Since all iterations of the algorithm take the same time,
we measured the iteration time in clock cycles for both the Versat and
the ARM Cortex A9 systems. The real execution time is given by the
number of clock cycles per iteration divided by the clock frequency in
the 40nm process SoC, according to table~\ref{tab_asic_r}.

The Versat results have been obtained by simulating one iteration of
the algorithm using behavioral RTL simulation. The Xilinx Zynq 7010
platform has been used to measure the performance of the ARM Cortex A9
core. The ARM results have been obtained using a system timer in a
bare metal implementation of the k-means clustering algorithm.

In figure~\ref{fig:timeVSdpts}, we show the iteration time as a
function of the number of datapoints. These results show that for both
Versat and the ARM core, the execution time scales linearly with the
number of datapoints. These numbers show a Versat/ARM average speedup
of 3.8, taking into account the number of clock cycles and the working
frequencies of the cores.

\begin{figure}[!htb]
  \centering
  \includegraphics[width=3.5in]{points.png}
  \caption{Iteration time vs. number of datapoints, for 34 centers and 30 dimensions.}
  \label{fig:timeVSdpts}
\end{figure}

In figure~\ref{fig:timeVScenters}, we show the execution time of one
iteration as a function of the number of centers. These results show
that for both Versat and the ARM core, the execution time scales
linearly with the number of centers, while the average speedup is
approximately 3.6. This speedup is slightly lower than in the previous
experiment because all datapoints can fit in the L2 cache (512kB) of
the ARM system.

\begin{figure}[!htb]
  \centering
  \includegraphics[width=3.5in]{centers.png}
  \caption{Iteration time vs. number of centers, for 10064 datapoints and 15 dimensions.}  
  \label{fig:timeVScenters}
\end{figure}

In figure~\ref{fig:timeVScoordinates}, we show the execution time of
one iteration as a function of the number of dimensions. From these
results, we see that the execution time per iteration increases
linearly with the number of coordinates, while the average speed up is
approximately the same. Unlike the other plots, this one shows some
non-linearity in the ARM curve, near the point where the size of the
L2 cache is exceeded. It is possible that this effect is also present
in figure~\ref{fig:timeVSdpts} for a low enough number of datapoints,
but it is not visible due to the scale of the horizontal axis. In
figure~\ref{fig:timeVScenters}, the L2 cache size is never exceeded,
which is consistent with the fact that Versat achieves a slightly
lower overall speedup of 3.6.

\begin{figure}[!htb]
  \centering
  \includegraphics[width=3.5in]{dims.png}
  \caption{Iteration time vs. number of dimensions, for 10064 datapoints and 34 centers.}  
  \label{fig:timeVScoordinates}
\end{figure}

The energy ratio $ER$ of using Versat compared to using the ARM Cortex
A9 processor, that is, the ratio of the energy consumed by the ARM
core and the energy consumed by Versat to execute the k-means
clustering algorithm, can be computed by
\begin{equation*}
  ER = SU \frac{Pa}{Pv} = 46.34,
\end{equation*}
where $SU=3.8$ is the average speedup, and $Pa$ and $Pv$ are the power
consumption of the ARM and Versat cores, respectively, as given in
table~\ref{tab_asic_r} for the 40nm process.


\section{Conclusion}
\label{sec:conc}

In this paper, a novel algorithm for k-means clustering targeting the
Versat CGRA has been presented. Although there are quite a few
solutions for accelerating this algorithm in GPUs and FPGAs, we have
not been able to find in the literature an algorithm for high
performance / ultra low energy embedded applications. For example, the
k-means clustering algorithm is useful in wireless sensor network
nodes, which are severely constrained in terms of compute capabilities
and energy consumption.

The Versat architecture has limited compute resources but it is
extremely flexible to program. In fact, Versat can be programmed like
a traditional controller, where the program itself creates and
partially reconfigures hardware datapaths in its data engine. The full
mesh topology of the data engine is easy to deal with by
programmers. Versat is capable of useful acceleration at low clock
rates and, given its small size it, can save orders of magnitude in
energy. It is designed to be a programmable alternative to dedicated
hardware accelerators, eliminating the risk of design errors.

This paper briefly outlines the Versat architecture and introduces new
architectural features, which can be used in other algorithms. The new
features are improvements to the Address Generation Units (AGUs) and
the Arithmetic and Logic Units (ALUs). The new AGU features are: (1)
AGUs can be bypassed and the memory port can use as address the value
present at the other port of the dual-port embedded memory; (2) the
address sequences produced by the AGUs can now be output and used in
the data engine for any other purposes. Feature (1) introduces pointer
support in Versat and feature (2) is especially useful for generating
synchronization signals for the functional units in the data
engine. The ALUs have been enhanced with conditional and cumulative
functions such as the accumulate and minimum functions, computed only
if a condition is true.

The proposed k-means clustering algorithm follows the usual assignment
and update steps. The algorithm uses one hardware datapath for each of
these steps: the assignment datapath labels all datapoints with the
index of the closest centroid, and the update datapath recalculates
the position of the centroids as the mean point of the datapoints
having the same label. The accumulate and minimum functions of the
ALUs are used to compute the minimum Manhattan distance, and the AGUs
of unused memory ports are used for synchronization. In the update
step, the datapoint labels are used as base addresses of the centroids
to update, bypassing the AGU of the memory port used to read the
centroids.

Our results show that Versat is 9.4x smaller than an ARM Cortex A9
processor, and runs the k-means algorithm 3.8x faster while consuming
46.3x less energy. It should be clear from these numbers that GPUs and
FPGAs cannot compete in this arena, and that the presented solution is
useful in applications where cost and energy consumption are crucial.

% use section* for acknowledgement
\section*{Acknowledgment}

This work was supported by national funds through Funda\c c\~ao para a
Ci\^encia e a Tecnologia (FCT) with reference UID/CEC/50021/2013.

\bibliographystyle{unsrt}
\bibliography{BIBfile}

% that's all folks
\end{document}


