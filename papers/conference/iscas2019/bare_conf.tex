\documentclass[journal]{IEEEtran}

% *** GRAPHICS RELATED PACKAGES ***
%
\ifCLASSINFOpdf
   \usepackage[pdftex]{graphicx}
  % declare the path(s) where your graphic files are
   \graphicspath{{./drawings/}}
  % and their extensions so you won't have to specify these with
  % every instance of \includegraphics
  % \DeclareGraphicsExtensions{.pdf,.jpeg,.png}
\else
  % or other class option (dvipsone, dvipdf, if not using dvips). graphicx
  % will default to the driver specified in the system graphics.cfg if no
  % driver is specified.
  % \usepackage[dvips]{graphicx}
  % declare the path(s) where your graphic files are
  % \graphicspath{{../eps/}}
  % and their extensions so you won't have to specify these with
  % every instance of \includegraphics
  % \DeclareGraphicsExtensions{.eps}
\fi


\usepackage[all]{nowidow}

% correct bad hyphenation here
\hyphenation{op-tical net-works semi-conduc-tor}

\newcommand{\at}{\makeatletter @\makeatother}

\begin{document}
%
% paper title
% Titles are generally capitalized except for words such as a, an, and, as,
% at, but, by, for, in, nor, of, on, or, the, to and up, which are usually
% not capitalized unless they are the first or last word of the title.
% Linebreaks \\ can be used within to get better formatting as desired.
% Do not put math or special symbols in the title.
\title{Low Energy Heterogeneous Computing with Multiple \mbox{RISC-V} and CGRA Cores}
%
%
% author names and IEEE memberships
% note positions of commas and nonbreaking spaces ( ~ ) LaTeX will not break
% a structure at a ~ so this keeps an author's name from being broken across
% two lines.
% use \thanks{} to gain access to the first footnote area
% a separate \thanks must be used for each paragraph as LaTeX2e's \thanks
% was not built to handle multiple paragraphs
%

\author{
  Lu\'is Fiolhais, Fernando Gon\c calves, Rui P. Duarte, M\'ario V\'estias$^1$, Jos\'e T. de Sousa\\
  INESC-ID/IST Lisboa, $^1$INESC-ID/ISEL, Email: jts\at inesc-id.pt
}

% note the % following the last \IEEEmembership and also \thanks - 
% these prevent an unwanted space from occurring between the last author name
% and the end of the author line. i.e., if you had this:
% 
% \author{....lastname \thanks{...} \thanks{...} }
%                     ^------------^------------^----Do not want these spaces!
%
% a space would be appended to the last name and could cause every name on that
% line to be shifted left slightly. This is one of those "LaTeX things". For
% instance, "\textbf{A} \textbf{B}" will typeset as "A B" not "AB". To get
% "AB" then you have to do: "\textbf{A}\textbf{B}"
% \thanks is no different in this regard, so shield the last } of each \thanks
% that ends a line with a % and do not let a space in before the next \thanks.
% Spaces after \IEEEmembership other than the last one are OK (and needed) as
% you are supposed to have spaces between the names. For what it is worth,
% this is a minor point as most people would not even notice if the said evil
% space somehow managed to creep in.

% make the title area
\maketitle

% As a general rule, do not put math, special symbols or citations
% in the abstract or keywords.
\begin{abstract}
In this paper, we introduce a heterogeneous computing platform consisting of
several \mbox{RISC-V} CPU and Versat CGRA cores. Implementation results for
several instances of the architecture are presented. The CPU of choice is the
promising open source \mbox{RISC-V} architecture. This paper presents
independent implementations of two \mbox{RISC-V} cores: a minimal one, useful as
a simple controller, and a higher performance 5-stage pipeline
implementation. The \mbox{RISC-V} cores have been designed using the recent
Chisel HDL, useful for automating tasks pertaining to the writing of RTL. The
selected CGRA is the published Versat architecture, for which 4 different
instances have been created. Implementation results for 2 FPGA families and ASIC
technology nodes are presented: area, frequency and power. The applications used
are digital audio encoders and decoders and demonstrate the competitiveness of
the approach in terms of silicon area area, frequency of operation and energy
consumption.
\end{abstract}

% Note that keywords are not normally used for peerreview papers.
\begin{IEEEkeywords}
Low power architectures, \mbox{RISC-V}, CGRAs, Heterogeneous computing
\end{IEEEkeywords}

% For peer review papers, you can put extra information on the cover
% page as needed:
% \ifCLASSOPTIONpeerreview
% \begin{center} \bfseries EDICS Category: 3-BBND \end{center}
% \fi
%
% For peerreview papers, this IEEEtran command inserts a page break and
% creates the second title. It will be ignored for other modes.
\IEEEpeerreviewmaketitle

\section{Introduction}

\IEEEPARstart{W}{ith} the advent of the Internet of Things (IoT) and Artificial
Intelligence (AI) algorithms based on neural networks, it is becoming more and
more important to reduce the cost and energy consumption of silicon devices.
Systems using high performance CPUs, GPUs, FPGAs or combinations of
these~\cite{Mittal15, Qiu16} are interesting but can hardly fit the bill in
applications requiring ubiquitous low cost and low power devices.

Most of the hardware of a high performance CPU is dedicated to handling
instructions efficiently, which represents a large overhead for the envisioned
systems. FPGAs are designed to build arbitrary digital circuits and require a
configuration infrastructure which is an overkill for low energy IoT
systems. GPUs comprise large numbers of streamlined von Neumann processors,
requiring a lot more hardware than a dedicated hardware datapath for the same
purpose. Finally, a system entirely implemented in hardware would probably have
the best performance but at the cost of a large silicon area (high device cost)
and lack of programmability.

Given the drawbacks of the above approaches, an interesting option is a system
consisting of several simple CPU cores combined with programmable hardware cores
that are simpler than FPGAs. For algorithms that require extensive control, the
use of a CPU can save area by time multiplexing multiple control tasks in the
same piece of hardware. Reasoning in a similar way, the data processing tasks
can be time multiplexed using programmable hardware: large hardware circuits can
be broken down into smaller pieces and run sequenntially on the programmable
hardware.

Because FPGAs are too onerous for most applications, a more suitable type of
reconfigurable hardware for embedded devices is the Coarse-Grained
Reconfigurable Array (CGRA)~\cite{deSutter10}. A CGRA is a collection of
programmable functional units (FUs) interconnected by programmable switches. The
FUs operate on data words of commonly used widths such as 8, 16, 32 or 64-bit
words. When the CGRA is programmed it implements hardware datapaths that
accelerate computations.

In this work, a heterogeneous computing platform consisting of several
\mbox{RISC-V} CPUs~\cite{riscv-isa-manual} and several Versat
CGRAs~\cite{Lopes16} is proposed. The \mbox{RISC-V} cores have been developed
under a project code named Adept. A similar approach has been proposed
in~\cite{hussain2014} but here we make the case for the use of \mbox{RISC-V}
CPUs and Versat CGRAs.

In order to sustain the IoT revolution and unleash the creativity of millions of
engineers, it is indispensable to have an Instruction Set Architecture that is
free of Intellectual Property (IP) rights or license
fees~\cite{asanovic14}. Among the few free Instruction Set Architectures (ISAs)
available, \mbox{RISC-V} seems to be the most promising one, with wide backing
from academia and industry. The other available open source solutions are either
dependent on specific technologies, have no parametrization options, or have
non-robust development environments~\cite{jts:rec14}.

With the emergence of \mbox{RISC-V}~\cite{riscv-isa-manual} and its open source
toolchain, with special emphasis on the Chisel Hardware Description Language
(HDL)~\cite{Bachrach:DAC2012} and the Rocket Chip SoC
Generator~\cite{Asanovic:EECS-2016-17}, the open source software model can be
extended to open CPU hardware descriptions.

A previous attempt to create a System on Chip (SoC) called Blackbird, and a
programming environment for building reconfigurable systems using the OpenRISC
open source processor has been made~\cite{jts:rec14}. Blackbird was intended to
become an independent SoC derived from ORPSoC~\cite{orpsoc}, the OpenRISC
equivalent of the Rocket Chip SoC Generator~\cite{Asanovic:EECS-2016-17}.
Unfortunately, the OpenRISC initiative has failed to create a mature enough
ecosystem and many essential parts of the system are either missing or still
being developed by the small existing community. In fact, in spite of the
initial enthusiasm, the same may happen to the \mbox{RISC-V}
initiative. However, its current impetus is far stronger than OpenRISC has ever
enjoyed, and today open source development is facilitated by Git enabled web
platforms.

The current project, called Warpbird~\cite{lf:rec18}, is the continuation of the
Blackbird project but uses \mbox{RISC-V} instead of OpenRISC processors. Similar
difficulties as when using the OpenRISC core, have surfaced. The existing RISC-V
open source projects proved not modular or documented enough and its components
cannot be easily be reused. In fact, it is arguably easier to write a
\mbox{RISC-V} core from scratch, and a \mbox{RISC-V} core called Adept has been
developed using the Chisel hardware description language. Adept is portable to
any FPGA or ASIC technology as Chisel generates synthesizable Verilog code. The
\mbox{RISC-V} CPU is supported by the GNU toolchain and the Versat CGRA has its
own compiler and assembler tools~\cite{Santiago2017}.

\section{Architecture}

The proposed architecture can be used to develop SoCs containing several Adept
\mbox{RISC-V} cores and Versat CGRAs. An example system consisting of 4
\mbox{RISC-V} cores and 2 Versat CGRAs is depicted in
Fig.~\ref{fig_top_sys}. The system uses ARM's Advanced eXtensible Interface
(AXI) to interconnect the cores, a de facto standard for busing.

\begin{figure}[!t]
\centering
\includegraphics[width=2in]{top_sys}
\caption{Block diagram of 4-CPU/2-CGRA instance}
\label{fig_top_sys}
\end{figure}

In the figure, two CPU cores share the same CGRA, which has a 2-port AXI slave
interface connected to the AXI master interfaces of its hosts. The CGRA
arbitrates between the two hosts using a round-robin scheme. All CPU and CGRA
cores access the external memory by means of their AXI master interfaces
connected to an AXI slave multi-port memory controller core, which is a 3rd
party core. The external memory must be loaded with programs and data for all
cores. This is done by the host CPU, which runs a boot loader program and is
able to fetch these data from an external device by means of an AXI or SPI
peripheral, not shown in the figure. The next subsections provide details about
the \mbox{RISC-V} CPU core and the Versat CGRA core.

\subsection{Adept}

Four \mbox{RISC-V} architectures have been studied in order to frame this work:
Taiga~\cite{matthews17}, a high performance \mbox{RISC-V} soft core; Rocket
Chip~\cite{Asanovic:EECS-2016-17}, the de facto standard used at UC Berkeley and
respective SiFive startup; PicoRV32~\cite{github-picorv32}, a size-optimized
architecture; and PULPino~\cite{gautschi17}, a single-core microcontroller
system. The proposed SoC in this work had a very specific set of requirements:
small size, low power, target FPGAs and ASICs and have a high abstraction level
description. Therefore, Rocket Chip was not selected because it had too many
features and proved too hard to manipulate; Taiga was not selected because it
targets FPGAs only; PULPino and PicoRV32 were not selected because they are
written in conventional HDL. In this project, the abstractions present in modern
programming languages are a requirement, namely to facilitate parametrization of
the RTL. Thus, after considering all 4 architectures, it was decided to
implement a new \mbox{RISC-V} core from scratch using the RV32IM ISA and the new
Chisel3 HDL~\cite{Bachrach:DAC2012}. The new architecture was called Adept.

There are two Adept configurations: Adept-3, a 3-stage pipeline low performance
configuration which implements the Base Integer (I) instruction set; and
Adept-5, a 5-stage pipeline medium performance configuration which implements
the Integer Multiplication and Division (IM) instruction sets. Both
configurations are optimized for silicon area and only support bare metal
applications. Both use instruction and data caches of sizes 8 and 16kB,
respectively.


\subsection{Versat}

The Versat architecture~\cite{Lopes16} is shown in
Fig.~\ref{fig_top_versat}. Versat uses the Controller to run programs and the
Data Engine (DE) to carry out data intensive computations. The Controller
programs are stored in the Program Memory (PM), which has a 1kB boot ROM and an
8kB user RAM. The Controller accesses the various modules in the system using
the Control Bus. The boot ROM program can load user programs into the PM from
the external memory or other device using the DMA or a special peripheral such
as an SPI core. The user program can generate DE configurations for the various
acceleration datapaths and store them in the Configuration Module (CM). It can
also move configurations between the CM and the external memory using the DMA
engine.

\begin{figure}[t]
\centering
\includegraphics[width=2.5in]{top_versat}
\caption{Versat top-level entity.}
\label{fig_top_versat}
\end{figure}

The Versat core has a host interface and a memory interface. The host interface
(AXI slave) is used by a host system to instruct Versat to load and execute
programs. Host and Versat communicate using the shared Control Register File
(CRF). The CRF is also used by Versat programs as a general purpose register
file. The memory interface (AXI master) is used by the DMA to access external
memory.

Different Versat instances can be generated containing different DE modules. The
DE consists of several Functional Units (FUs) interconnected in a full mesh. The
current FU types are: dual-port embedded memory, ALU (with and without internal
feedback to accumulate results), multiplier and barrel shifter.

The interconnect mesh has 1 clock cycle of delay and the FUs themselves are
pipelined for better performance. The FUs are used to build hardware datapaths
that work on data streams. Example datapaths are illustrated in
Fig.~\ref{fig_de_dp}. Datapath (a) in the figure is a simple pipeline exploiting
Instruction-Level Parallelism (ILP) by performing {\it load, add} and {\it
  store} instructions in parallel; datapath (b) demonstrates ILP plus Data-Level
Parallelism (DLP), or Thread-Level Parallelism (TLP) if the two additions are
operated as independent threads; datapath (c) involves more FUs thus achieving
more parallelism.

\begin{figure}[t]
\centering
\includegraphics[width=3.5in]{datapaths}
\caption{Data engine datapaths.}
\label{fig_de_dp}
\end{figure}

The full mesh structure may seem an overkill but it greatly simplifies Versat
programming, and dispenses with complex place and route algorithms. Due to the
relatively small number of FUs used, the full mesh interconnect becomes
affordable, occupying only 5\% of the chip area~\cite{Lopes16}.

When implemented in FPGAs, Versat is an overlay architecture, that is, it offers
faster reconfigurability on top of the FPGA programmability. When in implemented
in ASICs, Versat is a piece of programmable logic that reduces silicon area,
lowers the risk of design errors and saves energy while offering hardware-like
performance.

\section{Toolchain}
\label{sec:tchain}

\subsection{Adept}
The Adept \mbox{RISC-V} system has a fully operational GNU toolchain for
compilation, debugging and profiling. These are mature tools and most have been
merged upstream and their status can be checked in~\cite{riscv-tools-status} as
of version 7.1.  There are options to make compilation aware a the
floating-point unit, clock frequency for a specific board, etc.

Debug support using GDB is already available for \mbox{RISC-V} but Adept does
not yet have hardware support for it. A debug module and corresponding JTAG
interface have not yet been incorporated.

When working with the Rocket Chip SoC Generator, the debug module and its
software support via OpenOCD~\cite{openocd} were not inter-operable in the
Warpbird setup~\cite{lf:rec18}, and therefore could not be integrated in
Adept. The Rocket Chip debug module and JTAG interface needed to be made usable
in a different environment or new ones developed from scratch.

\subsection{Versat}

Being a CGRA, the Versat development tools are non-standard. Versat can be
programmed in assembly language~\cite{Lopes16} and using a C++
dialect~\cite{Santiago2017}.

Since the Versat Controller manages the Data Engine (DE) and the DMA engine via
the Control Bus, these modules can be programmed indirectly. To program the DE,
datapaths are written to the Configuration Module (CM). To operate the other
modules, the Versat Controller accesses their memory mapped registers, as
is usual when working with peripherals.

A debug tool for Versat has not yet been developed. Debugging a failing datapath
can be a difficult task and a combination of techniques must be employed. The
Versat Controller is instrumental in this process as it can start the DE and
stop it at desired instants, and can read and write to any position of the DE
memories. In extreme cases Versat must be debugged using RTL simulation or
Integrated Logic Analyzers.


\section{Results}
\label{sec:impl_results}

Results have been obtained for 4 different Versat instances described in
Table~\ref{tab_versat_configs}.

\begin{table}[t]
  \renewcommand{\arraystretch}{1.3}
  \caption{Versat Instances}
  \label{tab_versat_configs}
  \centering
  
    \begin{tabular}{|c||c|c|c|c|}
    \hline
    Instance & \#ALU & \#MUL & \#Barrel Shifter    & \#MEM \\
    \hline
    \hline
    Versat-1  &   6    &    4    &   1    &   4    \\  % Versat
    \hline
    Versat-2  &  14    &    5    &   5    &   5    \\  % HE-AAC encoder / AC3 encoder
    \hline
    Versat-3  &   8    &    4    &   4    &   5    \\  % 
    \hline
    Versat-4  &   8    &    8    &   0    &   5    \\  % AC3 decoder, HE-AAC decoder
    \hline
  \end{tabular}
\end{table}



\subsection{FPGA Implementation Results}

The FPGA implementation results for Intel ARRIA V and Xilinx KINTEX-7 devices
are shown in Tables~\ref{tab_fpga_versat_altera}
and~\ref{tab_fpga_versat_xilinx}, respectively.

The Adept processors are comparable to standard FPGA processors such as Microblaze
(Xilinx) or NIOS (Intel), and the Versat cores allow for compact implementations
compared to custom hardware unusable for multiple functions.

\begin{table}[t]
  \renewcommand{\arraystretch}{1.3}
  \caption{Versat FPGA implementation results for Arria V}
  \label{tab_fpga_versat_altera}
  \centering
    \begin{tabular}{|c||c|c|c|c|c|}
    \hline
    Instance & Logic & \#REG & RAM    & \#DSP & Fmax\\
             & (ALMs)  &      & (kbit) &       & (MHz)\\
    \hline
    \hline
    Adept-3  & 670     & 201   &  133 &   0 & 93  \\ % Cyclone V
    \hline
    Adept-5  & 1675    & 1470  &  266 &   4 & 120 \\ % Arria V
    \hline
    \hline
    Versat-1 &  8,607  & 4,673 & 351  &  32 & 130  \\  % Versat
    \hline
    Versat-2 & 15,340  & 12,620 & 623 &  25 & 120 \\  % HE-AAC encoder / AC3 encoder
    \hline
    Versat-3 & 13,700  & 11,270 & 623 &  21 & 120 \\  % HE-AAC decoder
    \hline
    Versat-4 & 10,680  & 11,200 & 623 &  36 & 120 \\  % AC3 decoder HE-AAC decoder
    \hline
  \end{tabular}
\end{table}

\begin{table}[t]
  \renewcommand{\arraystretch}{1.3}
  \caption{Versat FPGA implementation results for Kintex-7}
  \label{tab_fpga_versat_xilinx}
  \centering
  
   \begin{tabular}{|c||c|c|c|c|c|}
    \hline
    Instance & Logic  & \#REG  & RAM     & \#DSP & Fmax  \\
             & (LUTs) &        & (kbit)  &       & (MHz) \\
    \hline
    \hline
    Adept-3  & 1041   & 188    &  133    &  0    & 68 \\ % Zynq-7030-1
    \hline
    Adept-5  & 2533   & 1405   &  266    &  4    & 90 \\ % Kintex-7
    \hline
    \hline
    Versat-1 & 12,510 & 4,396  & 360     & 16    & 102 \\ % Versat
    \hline
    Versat-2 & 23,750 & 13,158 & 630     & 25    & 90  \\  % HE-AAC encoder / AC3 encoder
    \hline
    Versat-3 & 21,184 & 11,740 & 630     & 21    & 90  \\  % 
    \hline
    Versat-4 & 16,647 & 11,699 & 630     & 36    & 90  \\  % AC3 decoder, HE-AAC decoder
    \hline
  \end{tabular}
  
\end{table}

The results show that Versat cores can be implemented in different sizes with
minimal impact on the frequency of operation. However, due to the full mesh
structure, the frequency is limited to the 90-130 MHz range, depending on the
complexity of the instances.


\subsection{ASIC Implementation Results}

Versat and Adept have been implemented in the UMC 130 nm and TSMC 65 nm
processes.  Table~\ref{tab_asic_r} shows the results. The frequency and power
results have been obtained using the Cadence IC design and simulation tools.

\begin{table}[t]
  \renewcommand{\arraystretch}{1.3}
  \caption{Adept Integrated circuit implementation results.}
  \label{tab_asic_r}
  \centering
  \begin{tabular}{|c|c|c|c|c|c|}
    \hline
    Core       & Node & Area                    & Memory &  F    & Power\\
               & (nm) & (mm\textsuperscript{2}) & (kB)   & (MHz) & (mW)\\
    \hline
    \hline
    Adept-3    & 130   & 1.61     & 16  & 200     & 48 \\
    \hline
    Adept-3    & 65    & 0.40     & 16  & 400     & 24  \\
    \hline
    Adept-5    & 130   & 3.68     & 32  & 210     & 121.8 \\
    \hline
    Adept-5    & 65    & 0.92     & 32  & 420     & 58 \\
    \hline
    \hline
    Versat-1            & 130  & 5.20  & 46    & 90      &  132 \\
    \hline
    Versat-1            & 65   & 5.20  & 46    & 170     &  132 \\
    \hline
    Versat-2            & 130  &  9.52 & 76    & 256     &  355 \\
    \hline
    Versat-2            & 65   & 2.38  & 76    & 500     &  179 \\
    \hline
    Versat-3            & 130  & 8.54  & 76    & 261     &  330 \\
    \hline
    Versat-3            & 65   & 2.18  & 76    & 510     &  167 \\
    \hline
    Versat-4            & 130  & 8.30  & 68    & 258    &   321 \\
    \hline
    Versat-4            & 65   & 2.10  & 68    & 510     &  160 \\
    \hline
  \end{tabular}
\end{table}

The results show that the proposed heterogeneous computing platform is
competitive regarding silicon area and power consumption at least when compared
to an ARM Cortex A9~\cite{Lopes16}.

\subsubsection{Applications}

The applications that have been tested in the present architecture are digital
audio encoders and decoders and are outlined in Table~\ref{tab_apps}.

\begin{table}[t]
  \renewcommand{\arraystretch}{1.3}
  \caption{Audio encoders and decoders}
  \label{tab_apps}
  \centering

  \begin{tabular}{|c||c|c|}
    \hline
    Audio Format & \#CPUs & \#CGRAs \\
    \hline
    \hline
    MPEG1/2 Layers I/II encoder/decoder (2 chs) & 1 & 0 \\
    \hline
    AAC-LC stereo/multichannel decoder (6 chs)  & 1 & 0 \\
    \hline
    AAC-LC stereo encoder (2 chs) & 1           & 0 \\
    \hline
    AAC-LC multichannel encoder (6 chs)         & 3 & 0 \\
    \hline
    HE-AAC stereo encoder/decoder (2 chs)       & 1 & 1 \\
    \hline
    HE-AAC multichannel encoder/decoder (6 chs) & 3 & 1 \\
    \hline
    AC3 stereo encoder/decoder (2 chs)          & 1 & 1 \\
    \hline
    AC3 multichannel encoder/decoder (6 chs)    & 3 & 1 \\
    \hline
  \end{tabular}
\end{table}

The present platform is compared with an ARM
Cortex-A9~\cite{wang,arm-audio-presser} processor running the same
applications. Unfortunately, it was not possible to compare ~\cite{deSutter10}
because they were not available to be programmed with the same
applications. Comparisons with other CGRAs were made using published results
in~\cite{Lopes16}. The results obtained are shown in
Table~\ref{tab_apps_results} and have been normalized to 40nm to facilitate
direct comparison.

For the HE-AAC 5.1 audio decoder, the ARM system is 2.6x larger than the
proposed implementation and consumes 1.5x more power. For the HE-AAC 5.1 audio
encoder, the ARM system is 2.4x larger than the proposed implementation and
consumes 1.9x more power. The ARM Cortex A9 system uses the NEON SIMD unit
optimized at the assembly level. The ARM system needs a lower operating
frequency and less memory. However, the CGRA system uses much less logic
resources because CGRAs are data-centric and do not process instructions.


\begin{table}[t]
  \renewcommand{\arraystretch}{1.3}
  \caption{Implementation Comparison for HE-AAC 5.1 Audio Codecs}
  \label{tab_apps_results}
  \centering

  \begin{tabular}{|c||c|c|c|c|}
    \hline
                                    & Area                   & RAM  & Freq. & Power   \\
    Core (40nm) / Software          & (mm\textsuperscript{2})& (kB) & (MHz) & (mW)    \\
    \hline
    \hline
    ARM Cortex-A9 / Decoder         & 4.6                    & 64   & 50    & 31.25   \\
    \hline
    ARM Cortex-A9 / Encoder           & 4.6                    & 64   & 70    & 43.75   \\
    \hline
    \hline
    3xAdept-5 + Versat-4 / Decoder    & 1.75                   & 164  & 80    & 20.76   \\
    \hline
    3xAdept-5 + Versat-2 / Encoder    & 1.95                   & 172  & 80    & 23.31   \\
    \hline
  \end{tabular}
\end{table}


\section{Conclusion}

In this work a low power heterogeneous system is proposed, composed of
\mbox{RISC-V} processors and Versat CGRA cores, targeting any design flow (FPGA
or ASIC). The designed \mbox{RISC-V} core, Adept, has two configurations: a
small 3-stage pipeline configuration, and a more performant 5-stage pipeline
configuration. The Versat CGRA allows for a large set of applications to be
executed with minimal energy requirements and high performance.

The \mbox{RISC-V} ISA enables the development of SoCs that use one or more
processor instances, free of license fees or royalties. Moreover, the
\mbox{RISC-V} ISA is supported by a rich open source toolchain and a thriving
developer community. In this work, it was concluded that it is still better to
develop the RTL in-house as the available open source projects are difficult to
manipulate and extend.

When implemented in FPGA, the present platform constitutes an overlay
architecture, offering fast on-the-fly reconfiguration on top of the FPGA
configurability. When in implemented in ASIC the system is field programmable.

The implementation results show that it is possible to instantiate several
\mbox{RISC-V} and Versat cores in mid-range FPGAs and run real-world
applications such as the audio encoders and decoders presented herein. When
implemented as an ASIC, the proposed heterogeneous computing platform consumes
close to 3x less silicon area and 2x less energy compared to an ARM Cortex-A9
system equipped with the NEON SIMD unit.

% use section* for acknowledgment
\section*{Acknowledgment}

This work was supported by national funds through Funda\c c\~ao para a Ci\^encia
e a Tecnologia (FCT) under projects PTDC/EEI-HAC/30848/2017 and
UID/CEC/50021/2019.

% Can use something like this to put references on a page
% by themselves when using endfloat and the captionsoff option.
\ifCLASSOPTIONcaptionsoff
  \newpage
\fi

% trigger a \newpage just before the given reference
% number - used to balance the columns on the last page
% adjust value as needed - may need to be readjusted if
% the document is modified later
%\IEEEtriggeratref{8}
% The "triggered" command can be changed if desired:
%\IEEEtriggercmd{\enlargethispage{-5in}}

% references section

% can use a bibliography generated by BibTeX as a .bbl file
% BibTeX documentation can be easily obtained at:
% http://mirror.ctan.org/biblio/bibtex/contrib/doc/
% The IEEEtran BibTeX style support page is at:
% http://www.michaelshell.org/tex/ieeetran/bibtex/
%\bibliographystyle{IEEEtran}
% argument is your BibTeX string definitions and bibliography database(s)
%\bibliography{IEEEabrv,../bib/paper}
%
% <OR> manually copy in the resultant .bbl file
% set second argument of \begin to the number of references
% (used to reserve space for the reference number labels box)

\bibliographystyle{unsrt}
\bibliography{./bibtex/BIBfile}

% biography section
% 
% If you have an EPS/PDF photo (graphicx package needed) extra braces are
% needed around the contents of the optional argument to biography to prevent
% the LaTeX parser from getting confused when it sees the complicated
% \includegraphics command within an optional argument. (You could create
% your own custom macro containing the \includegraphics command to make things
% simpler here.)
%\begin{IEEEbiography}[{\includegraphics[width=1in,height=1.25in,clip,keepaspectratio]{mshell}}]{Michael Shell}
% or if you just want to reserve a space for a photo:


% that's all folks
\end{document}



%%  LocalWords:  parametrization
